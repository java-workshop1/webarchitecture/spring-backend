INSERT INTO TODO_LIST (ID, NAME) VALUES
  ('1234', 'Productivity'),
  ('231asd', 'Shopping'),
  ('ewafadsa', 'Reminders');

INSERT INTO TODO (ID, DESCRIPTION, DONE, TODO_LIST_ID) VALUES
  ('1234', 'Sleep', 0, '1234'),
  ('12345', 'Learn Java', 0, '1234'),
  ('1241431', 'Learn Spring', 0, '1234'),
  ('512312541', 'Get up', 1, '1234'),
  ('42321412312', 'Meat', 1, '231asd'),
  ('45123', 'Salad', 0, '231asd'),
  ('321413', 'My Birthday', 1, 'ewafadsa'),
  ('321adjsh', 'Work', 0, 'ewafadsa'),
  ('asdasfasd', 'Learn Java', 0, 'ewafadsa');
