package com.infactory.workshop.backend.repository;

import java.util.List;

import com.infactory.workshop.backend.model.Todo;
import com.infactory.workshop.backend.model.TodoList;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoRepository extends JpaRepository<Todo, String> {

  List<Todo> findAllByTodoList(TodoList todoList);

  List<Todo> findAllByTodoListId(String todoListId);

}
