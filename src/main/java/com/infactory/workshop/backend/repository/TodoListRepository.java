package com.infactory.workshop.backend.repository;

import com.infactory.workshop.backend.model.TodoList;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoListRepository extends JpaRepository<TodoList, String> {
}
