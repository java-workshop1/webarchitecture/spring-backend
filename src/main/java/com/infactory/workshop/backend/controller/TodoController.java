package com.infactory.workshop.backend.controller;

import java.util.List;

import com.infactory.workshop.backend.model.Todo;
import com.infactory.workshop.backend.service.TodoService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "/api/lists/{listId}/todos")
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class TodoController {

  private TodoService todoService;

  @GetMapping
  public List<Todo> getAll(@PathVariable String listId) {
    return todoService.getAllByListId(listId);
  }

  @PostMapping
  public ResponseEntity<?> create(@PathVariable String listId, @RequestBody Todo todo, UriComponentsBuilder b) {
    Todo result = todoService.create(listId, todo);
    return ResponseEntity
        .created(b.path("/api/lists/{listId}/todos/{todoId}").buildAndExpand(listId, result.getId()).toUri()).build();
  }

  @PutMapping("/{id}")
  public ResponseEntity<?> update(@PathVariable String listId, @PathVariable String id, @RequestBody Todo todo) {
    todoService.update(listId, todo);
    return ResponseEntity.noContent().build();
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> delete(@PathVariable String listId, @PathVariable String id) {
    todoService.delete(id);
    return ResponseEntity.noContent().build();
  }
}
