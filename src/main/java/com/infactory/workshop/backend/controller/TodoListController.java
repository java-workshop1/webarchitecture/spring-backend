package com.infactory.workshop.backend.controller;

import java.util.List;

import com.infactory.workshop.backend.model.TodoList;
import com.infactory.workshop.backend.service.TodoListService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "/api/lists")
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class TodoListController {

  private TodoListService todoListService;

  @GetMapping
  public List<TodoList> getAll() {
    return todoListService.getAll();
  }

  @PostMapping
  public ResponseEntity<?> create(@RequestBody TodoList todoList, UriComponentsBuilder b) {
    TodoList result = todoListService.createTodoList(todoList);
    return ResponseEntity.created(b.path("/api/lists/{id}").buildAndExpand(result.getId()).toUri()).build();
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> delete(@PathVariable String id) {
    todoListService.deleteTodoList(id);
    return ResponseEntity.noContent().build();
  }
}
