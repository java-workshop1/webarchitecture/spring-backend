package com.infactory.workshop.backend.service;

import java.util.List;

import com.infactory.workshop.backend.model.TodoList;

public interface TodoListService {
  List<TodoList> getAll();

  TodoList createTodoList(TodoList todoList);

  void deleteTodoList(String id);
}
