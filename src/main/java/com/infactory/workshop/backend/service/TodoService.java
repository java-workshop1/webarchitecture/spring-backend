package com.infactory.workshop.backend.service;

import java.util.List;

import com.infactory.workshop.backend.model.Todo;

public interface TodoService {
  List<Todo> getAllByListId(String listId);

  Todo create(String todoListId, Todo todo);

  void delete(String id);

  Todo update(String todoListId, Todo todo);
}
