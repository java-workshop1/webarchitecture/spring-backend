package com.infactory.workshop.backend.service;

import java.util.List;

import com.infactory.workshop.backend.model.Todo;
import com.infactory.workshop.backend.repository.TodoListRepository;
import com.infactory.workshop.backend.repository.TodoRepository;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.val;

@Service
@AllArgsConstructor
public class TodoServiceImpl implements TodoService {

  private TodoRepository todoRepository;
  private TodoListRepository todoListRepository;

  @Override
  public List<Todo> getAllByListId(String listId) {
    return todoRepository.findAllByTodoListId(listId);
  }

  @Override
  public Todo create(String todoListId, Todo todo) {
    val list = todoListRepository.findById(todoListId);
    todo.setTodoList(list.get());
    return todoRepository.save(todo);
  }

  @Override
  public void delete(String id) {
    todoRepository.deleteById(id);
  }

  @Override
  public Todo update(String todoListId, Todo todo) {
    val list = todoListRepository.findById(todoListId);
    todo.setTodoList(list.get());
    return todoRepository.save(todo);
  }

}
