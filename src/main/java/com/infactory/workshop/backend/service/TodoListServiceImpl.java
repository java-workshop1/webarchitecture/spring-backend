package com.infactory.workshop.backend.service;

import java.util.List;

import com.infactory.workshop.backend.model.TodoList;
import com.infactory.workshop.backend.repository.TodoListRepository;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class TodoListServiceImpl implements TodoListService {

  private TodoListRepository todoListRepository;

  @Override
  public List<TodoList> getAll() {
    return todoListRepository.findAll();
  }

  @Override
  public TodoList createTodoList(TodoList todoList) {
    return todoListRepository.save(todoList);
  }

  @Override
  public void deleteTodoList(String id) {
    todoListRepository.deleteById(id);
  }

}
