package com.infactory.workshop.backend.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class Todo {
  @Id
  private String id;
  private String description;
  private boolean done;
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "todo_list_id", nullable = false)
  @JsonIgnore
  private TodoList todoList;
}
