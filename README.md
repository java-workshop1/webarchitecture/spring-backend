# Endpoints
List of endpoints, that the frontend expects.

## Todo Lists

- GET http://localhost:8000/api/lists
- POST http://localhost:8000/api/lists
- DELETE http://localhost:8000/api/lists/:id

## Todos

- GET http://localhost:8000/api/lists/:id/todos
- POST http://localhost:8000/api/lists/:id/todos
- DELETE http://localhost:8000/api/lists/:id/todos/:id2
- PUT http://localhost:8000/api/lists/:id/todos/:id2

# Data Model
List of data models that are used in the frontend.

## Todo List
```
{
    id: uuid/string,
    name: string
    todos: []@Todo
}
```

## Todo
```
{
    id: uuid/string,
    description: string,
    done: boolean,
}
```

## Business Logic
Everything is done by the backend, except for ID generation. It is done by the frontend with UUIDs.
The Backend should expect that in every payload an ID in the form of a UUID is provided.
